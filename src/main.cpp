#include "../config.h"

#include <QtGui/QApplication>
#include <QtDBus/QDBusConnection>
#include <locale.h>
#include <glib/gi18n.h>

#include "bloom-widget-bookmarks.h"
#include "favorite-widget.h"
#include "bloom-widget-dbus-adaptor.h"
#include "bloom-widget-qt.h"

#define _(String) gettext (String)

#define BLOOM_WIDGET_BOOKMARKS_SERVICE "org.agorabox.Bloom.Widget.Bookmarks"

int main(int argc, char **argv) {
    QString object_path;
    QString service_name;
    QDBusConnection connection = QDBusConnection::sessionBus();

    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

    QApplication app(argc, argv);

    if (argc == 2 && strcmp(argv[1], "-s") == 0) {
        FavoriteWidget standAlone("");
        standAlone.show();
        return app.exec();
    }

    BloomWidgetBookmarks *widget = new BloomWidgetBookmarks();
    new WidgetAdaptor(widget);

    QString service = get_free_service_name(BLOOM_WIDGET_BOOKMARKS_SERVICE);
    connection.registerService(service);
    connection.registerObject("/Widget", widget);

    return app.exec();
}

