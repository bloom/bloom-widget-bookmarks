#include "favorite-widget.h"
#include <QDebug>

FavoriteWidget *favorite_widget = NULL;

FavoriteWidget::FavoriteWidget(QString gconf_entry, QWidget *parent) : QX11EmbedWidget(parent)
{
    favorite_widget = this;
    shortcuts = new GConfItem(gconf_entry + "/shortcuts");
    global_layout = new QVBoxLayout(this);
    scroll_widget = new QScrollArea();
    sub_widget = new QLabel(" ");
    main_widget = new QWidget(scroll_widget);
    main_layout = new QGridLayout(main_widget);
    font = new QFont("Arial", 8);
    font_metrics = new QFontMetrics(*font);

    this->setObjectName("GlobalWidget");

    global_layout->setMargin(0);
    global_layout->setSpacing(GLOBAL_LAYOUT_SPACING);

    sub_widget->setFont(*font);
    sub_widget->setFixedHeight(APP_NAME_SIZE);
    sub_widget->setContentsMargins(APP_NAME_MARGIN, 0, APP_NAME_MARGIN, 0);
    sub_widget->setObjectName("SubWidget");

    main_widget->setContentsMargins(CONTENT_MARGINS, CONTENT_MARGINS, CONTENT_MARGINS, CONTENT_MARGINS);
    //main_widget->setMinimumWidth(BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS);
    //main_widget->setMaximumHeight(BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS);
    main_widget->setObjectName("BookmarkWidget");

    main_layout->setMargin(0);
    main_layout->setSpacing(BOOKMARK_BUTTON_MARGINS);
    main_layout->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    scroll_widget->setObjectName("BookmarkWidget_scroll");
    //scroll_widget->setContentsMargins(0, 0, 0, 0);
    scroll_widget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //scroll_widget->setMinimumWidth(BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS + BOOKMARK_SCROLLBAR_WIDTH + CONTENT_MARGINS);

    animation_widget = new QLabel(this);
    animation_widget->setScaledContents(TRUE);
    animation_widget->resize(BOOKMARK_BUTTON_SIZE, BOOKMARK_BUTTON_SIZE);
    animation_widget->setAttribute(Qt::WA_TranslucentBackground);
    animation_widget->setWindowFlags(Qt::ToolTip);
    animation_widget->hide();
    
    // Apply CSS file
    QFile f(PKGDATADIR"/bookmarkbutton.css");
    if (f.open(QIODevice::ReadOnly)) {
        setStyleSheet(f.readAll().replace(QByteArray("IMG_DIR"), QByteArray(PKGDATADIR)));
    }

    QStringList l_shortcuts = shortcuts->value().toStringList();

    if (!l_shortcuts.size()) {
        manager = mpl_app_bookmark_manager_get_default ();
        GList *l;
        GList *uris;
        uris = mpl_app_bookmark_manager_get_bookmarks(manager);

        for (l = uris; l; l = l->next)
            l_shortcuts.append((char *) l->data);

        // detect signals from mutter bloom to add or remove bookmarks
        // this method will be change with Gconf signal
        g_signal_connect(manager,
                         "bookmark-added",
                         GCallback(fav_applications_add),
                         this);


        g_signal_connect(manager,
                         "bookmark-removed",
                         GCallback(fav_applications_remove),
                         this);


        g_signal_connect(manager,
                         "bookmark-changed",
                         GCallback(fav_applications_reset),
                         this);
    }
    else {
        l_shortcuts = shortcuts->value().toStringList();
    }

    for (int i = 0; i< l_shortcuts.size(); ++i)
    {
        QString shortcut = l_shortcuts.at(i);
        shortcut = shortcut.right(shortcut.size() - 7);
        BookmarkButton *but = new BookmarkButton(scroll_widget, main_widget);
        but->setApplication(mnb_launcher_application_new_from_desktop_file((const gchar*) shortcut.toLatin1()));
        but->set_sub_widget(sub_widget);
        but->set_animation_widget(animation_widget);
        but->setFontMetrics(font_metrics);
        main_layout->addWidget(but, i, 0);
        connect(but, SIGNAL(button_removed(const gchar *)), this, SLOT(slot_bookmark_removed(const gchar *)));
    }
    has_scroll = true;
    main_widget->setLayout(main_layout);
    scroll_widget->setWidget(main_widget);
    global_layout->addWidget(scroll_widget);
    global_layout->addWidget(sub_widget);

    animation_widget->raise();

    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
}

FavoriteWidget::~FavoriteWidget()
{
    delete shortcuts;
}

void FavoriteWidget::resizeGrid(int width)
{
    if (this->has_scroll == true)
        width -= BOOKMARK_SCROLLBAR_WIDTH;

    QList<BookmarkButton *> tmp_widget_list;
    int widget_size = int (width / (BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS + CONTENT_MARGINS));
    int nb_horizontal_widgets = 0;
    int nb_vertical_widgets = 0;

    for (int i=0; i < main_layout->count(); ++i) {
        tmp_widget_list.append(qobject_cast<BookmarkButton *>(main_layout->itemAt(i)->widget()));
    }
    for (int i=0; i < tmp_widget_list.length(); ++i) {
        tmp_widget_list.at(i)->setPos(new QPoint((i % widget_size), int(i / widget_size)));
        main_layout->addWidget(tmp_widget_list.at(i), int(i / widget_size), (i % widget_size));

        if (int(i / widget_size) > nb_vertical_widgets)
            nb_vertical_widgets = int(i / widget_size);
        if (i % widget_size > nb_horizontal_widgets)
            nb_horizontal_widgets = i % widget_size;
    }
    main_widget->setFixedHeight((nb_vertical_widgets + 1) * (BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS) + (CONTENT_MARGINS * 2));
    main_widget->setFixedWidth((nb_horizontal_widgets + 1) * (BOOKMARK_BUTTON_SIZE + BOOKMARK_BUTTON_MARGINS) + (CONTENT_MARGINS * 2));
}

void FavoriteWidget::resizeEvent(QResizeEvent *ev)
{
    this->scroll_widget->setFixedSize(this->width(), this->height() - this->sub_widget->height() - GLOBAL_LAYOUT_SPACING);
    resizeGrid(this->scroll_widget->width());
    if (this->has_scroll == false && this->main_widget->height() > this->scroll_widget->height()) {
        this->has_scroll = true;
        this->resizeGrid(this->scroll_widget->width());
    }
    else if (this->has_scroll == true && this->main_widget->height() < this->scroll_widget->height()) {
        this->has_scroll = false;
        this->resizeGrid(this->scroll_widget->width());
    }
    
    this->sub_widget->setFixedSize(this->scroll_widget->width(), APP_NAME_SIZE);
}

void FavoriteWidget::slot_bookmark_removed(const gchar *uri)
{
    mpl_app_bookmark_manager_remove_uri(manager, QString("file://" + QString(uri)).toStdString().c_str());
}

void
FavoriteWidget::changed_bookmark()
{
    mutex.lock();
    QList<BookmarkButton *> tmp_widget_list;
    
    for (int i=0; i < main_layout->count(); ++i) {
        tmp_widget_list.append(qobject_cast<BookmarkButton *>(main_layout->itemAt(i)->widget()));
    }

    while (!tmp_widget_list.isEmpty()) {
        main_layout->removeWidget(tmp_widget_list.at(0));
        if (tmp_widget_list.at(0) != NULL) delete tmp_widget_list.at(0);
        tmp_widget_list.removeFirst();
    }

    if (!main_layout->isEmpty()) {
        QLayoutItem *child;
        while ((child = main_layout->takeAt(0)) != 0)
            delete child;
    }

	QList<const gchar*> appList;
    GList *l;
    GList *uris;
    uris = mpl_app_bookmark_manager_get_bookmarks(manager);

    for (l = uris; l; l = l->next)
        appList.append((char *)l->data);

    for (int i=0; i< appList.size(); ++i)
    {
        BookmarkButton *but = new BookmarkButton(scroll_widget, main_widget);
        but->setApplication(mnb_launcher_application_new_from_desktop_file(appList.at(i) + (7*sizeof(char))));
        but->set_sub_widget(sub_widget);
        but->setFontMetrics(font_metrics);
        but->set_animation_widget(animation_widget);
        main_layout->addWidget(but, 0, i);

        connect(but, SIGNAL(button_removed(const gchar *)), this, SLOT(slot_bookmark_removed(const gchar *)));
    }

    resizeGrid(this->scroll_widget->width());
    mutex.unlock();
}

void
FavoriteWidget::removed_bookmark(const gchar *uri)
{
    /*mutex.lock();
    if (bookmark_list.size() < 2) {
        mutex.unlock();
        changed_bookmark(false);
        return;
    }

    int i;

    for (int i = 0; i < bookmark_list.size(); ++i) {
        if (g_strcmp0(bookmark_list.at(i)->get_uri(), uri))
            continue;
        favourite_Glay->removeWidget(bookmark_list.at(i));
        if (bookmark_list.at(i) != NULL) delete bookmark_list.at(i);
        bookmark_list.removeAt(i);
        break;
    }

    if (bookmark_list.size() != favourite_Glay->count() ||
        bookmark_list.size() != g_list_length (mpl_app_bookmark_manager_get_bookmarks(priv->manager))) {
        if (favourite_Glay->count() != 1 || bookmark_list.size() != 0 ||
            g_list_length (mpl_app_bookmark_manager_get_bookmarks(priv->manager)) != 0) {
            mutex.unlock();
            changed_bookmark(false);
            return;
        }
    }
    mutex.unlock();*/
}

void
FavoriteWidget::added_bookmark(const gchar *uri)
{
    /*mutex.lock();
    if (bookmark_list.size() < 2) {
        mutex.unlock();
        changed_bookmark(false);
        return;
    }

    int i;

    for (int i = 0; i < bookmark_list.size(); ++i) {
        if (!g_strcmp0(bookmark_list.at(i)->get_uri(), uri)) {
            mutex.unlock();
            return;
        }
    }
    
    widget_appli *widgetAppli = new widget_appli();
    widgetAppli->setFixedHeight(80);
    MnbLauncherApplication *mla_from_file = mnb_launcher_application_new_from_desktop_file(uri + (7*sizeof(char)));
    if (launcher_button_create_from_entry (widgetAppli, (MnbLauncherApplication *) mla_from_file, uri, priv->theme)) {
        favourite_Glay->addWidget(widgetAppli);
        bookmark_list.append(widgetAppli);
    }

    if (bookmark_list.size() != favourite_Glay->count() ||
        bookmark_list.size() != g_list_length (mpl_app_bookmark_manager_get_bookmarks(priv->manager))) {
        if (favourite_Glay->count() != 1 || bookmark_list.size() != 0 ||
            g_list_length (mpl_app_bookmark_manager_get_bookmarks(priv->manager)) != 0) {
            mutex.unlock();
            changed_bookmark(false);
            return;
        }
    }
    mutex.unlock();*/
}

void fav_applications_reset (MplAppBookmarkManager *manager, const gchar *userdata)
{
    favorite_widget->changed_bookmark();
}

void fav_applications_remove (MplAppBookmarkManager *manager, const gchar *userdata)
{
    favorite_widget->changed_bookmark();
}

void fav_applications_add (MplAppBookmarkManager *manager, const gchar *userdata)
{
    favorite_widget->changed_bookmark();
}

