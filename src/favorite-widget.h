#ifndef WIDGET_H
#define WIDGET_H

#include <QtGui/QWidget>
#include <QGridLayout>
#include <QResizeEvent>
#include <QScrollArea>
#include <QFile>
#include <QX11EmbedWidget>
#include <QtGui>

#include "bookmark-button.h"
#include "mnb-launcher-application.h"
#include "gconfitem.h"

extern "C"
{
    #include <moblin-panel/mpl-app-bookmark-manager.h>
}

#define CONTENT_MARGINS 0
#define TOOLTIP_ARROW_WIDTH 13
#define TOOLTIP_ARROW_HEIGHT 7
#define BOOKMARK_SCROLLBAR_WIDTH 16
#define APP_NAME_SIZE   14
#define APP_NAME_MARGIN 5
#define GLOBAL_LAYOUT_SPACING   1

class FavoriteWidget : public QX11EmbedWidget
{
    Q_OBJECT

public:
                        FavoriteWidget(QString gconf_entry, QWidget *parent = 0);
                        ~FavoriteWidget();
    void                changed_bookmark();
    void                removed_bookmark(const gchar*);
    void                added_bookmark(const gchar*);

private:
    QVBoxLayout         *global_layout;
    QGridLayout         *main_layout;

    QScrollArea         *scroll_widget;
    QWidget             *main_widget;
    QLabel              *sub_widget;
    bool                has_scroll;

    QLabel              *animation_widget;

    QFont               *font;
    QFontMetrics        *font_metrics;
    QMutex               mutex;
    GConfItem           *shortcuts;

    MplAppBookmarkManager   *manager;

protected:
    void                resizeEvent(QResizeEvent*);
    void                resizeGrid(int);

public slots:
    void                slot_bookmark_removed(const gchar *);
};

void fav_applications_reset (MplAppBookmarkManager *manager, const gchar *userdata);
void fav_applications_remove (MplAppBookmarkManager *manager, const gchar *userdata);
void fav_applications_add (MplAppBookmarkManager *manager, const gchar *userdata);

#endif // WIDGET_H
