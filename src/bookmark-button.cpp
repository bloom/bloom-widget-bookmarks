#include "bookmark-button.h"
#include <QDebug>

BookmarkButton::BookmarkButton(QWidget *parent, QWidget *resizer) : QPushButton(parent), resizer(resizer)
{
    setObjectName("bookmark_button");
    button_layout = new QVBoxLayout(this);
    button_pos = new QPoint(0, 0);
    
    button_layout->setMargin(0);
    button_layout->setAlignment(Qt::AlignCenter);
    setFocusPolicy(Qt::NoFocus);
    setFixedSize(BOOKMARK_BUTTON_SIZE, BOOKMARK_BUTTON_SIZE);

    setLayout(button_layout);
}

void BookmarkButton::setApplication(MnbLauncherApplication* app)
{
    application = app;
    image = new QPixmap( QString::fromUtf8(launcher_button_get_icon_file(mnb_launcher_application_get_icon(app), gtk_icon_theme_get_default())) );

    widget_image = new QLabel;
    widget_image->setObjectName("bookmark_widget_image");
    widget_image->setScaledContents(TRUE);
    widget_image->setPixmap(image->scaled(BOOKMARK_BUTTON_IMAGE_SIZE, BOOKMARK_BUTTON_IMAGE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    widget_image->setMaximumSize(BOOKMARK_BUTTON_IMAGE_SIZE, BOOKMARK_BUTTON_IMAGE_SIZE);
    widget_image->setContentsMargins(0, 0, 0, 0);

    button_layout->addWidget(widget_image);

    remove_button = new QPushButton(this);
    remove_button->setObjectName("remove_button");
    remove_button->setFocusPolicy(Qt::NoFocus);
    remove_button->setFixedSize(BOOKMARK_BUTTON_REMOVE_SIZE, BOOKMARK_BUTTON_REMOVE_SIZE);
    remove_button->setGeometry(BOOKMARK_BUTTON_SIZE - BOOKMARK_BUTTON_REMOVE_SIZE -2, 2, BOOKMARK_BUTTON_REMOVE_SIZE, BOOKMARK_BUTTON_REMOVE_SIZE);
    remove_button->hide();

    application_name = QString::fromUtf8( mnb_launcher_application_get_name(application));

    int pos = (BOOKMARK_BUTTON_SIZE - BOOKMARK_BUTTON_IMAGE_SIZE) / 2;
    
    anim_enter = new QPropertyAnimation(widget_image, "geometry", this);
    anim_enter->setDuration(150);
    anim_enter->setStartValue(QRect(pos, pos, BOOKMARK_BUTTON_IMAGE_SIZE, BOOKMARK_BUTTON_IMAGE_SIZE));
    anim_enter->setEndValue(QRect(pos + 3, pos + 3, BOOKMARK_BUTTON_IMAGE_SIZE - 6, BOOKMARK_BUTTON_IMAGE_SIZE - 6));
    
    anim_leave = new QPropertyAnimation(widget_image, "geometry", this);
    anim_leave->setDuration(150);
    anim_leave->setStartValue(QRect(pos + 3, pos + 3, BOOKMARK_BUTTON_IMAGE_SIZE - 6, BOOKMARK_BUTTON_IMAGE_SIZE - 6));
    anim_leave->setEndValue(QRect(pos, pos, BOOKMARK_BUTTON_IMAGE_SIZE, BOOKMARK_BUTTON_IMAGE_SIZE));
    
    connect(remove_button, SIGNAL(clicked()), this, SLOT(slot_delete_favorite()));
}

MnbLauncherApplication* BookmarkButton::getApplication() const
{
    return application;
}

void BookmarkButton::setPos(QPoint *pos)
{
    delete button_pos;
    button_pos = pos;
}

void BookmarkButton::set_sub_widget(QLabel *lbl)
{
    sub_widget = lbl;
}

void BookmarkButton::set_animation_widget(QLabel *widget)
{
    animation_widget = widget;
}

void BookmarkButton::setFontMetrics(QFontMetrics *met)
{
    font_metrics = met;
}

void BookmarkButton::enterEvent(QEvent *e)
{
    sub_widget->setText(font_metrics->elidedText(this->application_name, Qt::ElideRight, this->sub_widget->width() - (2 * APP_NAME_MARGIN)));
    remove_button->show();

    anim_enter->start();
}

void BookmarkButton::leaveEvent(QEvent *e)
{
    sub_widget->setText("");
    remove_button->hide();

    anim_leave->start();
}

void BookmarkButton::launch_favorite()
{
    animation_widget->setPixmap(image->scaled(BOOKMARK_BUTTON_IMAGE_SIZE - 10, BOOKMARK_BUTTON_IMAGE_SIZE - 10, Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
    animation_widget->show();

    group_launch = new QParallelAnimationGroup;
    
    connect(this->group_launch, SIGNAL(finished()), this, SLOT(slot_launch_animation_finished()));

    anim_launch_opacity = new QPropertyAnimation(animation_widget, "windowOpacity");
    anim_launch_opacity->setDuration(200);
    anim_launch_opacity->setStartValue(1);
    anim_launch_opacity->setEndValue(0);

    anim_launch_geometry = new QPropertyAnimation(animation_widget, "geometry");
    anim_launch_geometry->setDuration(200);
    anim_launch_geometry->setStartValue(QRect(mapToGlobal(QPoint(0,0)).x() + (BOOKMARK_BUTTON_SIZE / 2), mapToGlobal(QPoint(0,0)).y() + (BOOKMARK_BUTTON_SIZE / 2), 1, 1));
    anim_launch_geometry->setEndValue(QRect(mapToGlobal(QPoint(0,0)).x() - 50, mapToGlobal(QPoint(0,0)).y() - 50, BOOKMARK_BUTTON_SIZE + 100, BOOKMARK_BUTTON_SIZE + 100));

    group_launch->addAnimation(anim_launch_geometry);
    group_launch->addAnimation(anim_launch_opacity);

    group_launch->start();
}

void BookmarkButton::mouseReleaseEvent(QMouseEvent *event)
{
    launch_favorite();
}

void BookmarkButton::slot_delete_favorite()
{
    emit button_removed(mnb_launcher_application_get_desktop_file(application));
}

void BookmarkButton::slot_launch_animation_finished()
{
    animation_widget->hide();
    delete group_launch;

    GAppLaunchContext    *ctx;
    GdkAppLaunchContext  *gctx;
    GAppInfo             *app;
    GError               *error = NULL;
    gboolean              retval = TRUE;

    gctx = gdk_app_launch_context_new ();
    ctx  = G_APP_LAUNCH_CONTEXT (gctx);

    const gchar *executable = mnb_launcher_application_get_executable(application);
    if (executable) {
        app = g_app_info_create_from_commandline(executable, NULL, G_APP_INFO_CREATE_SUPPORTS_URIS, &error);
    }
    else {
        const gchar *url = mnb_launcher_application_get_url(application);
        gchar *cmd = g_strconcat("gnome-open ", url, NULL);
        app = g_app_info_create_from_commandline(cmd, NULL, G_APP_INFO_CREATE_SUPPORTS_URIS, &error);
        g_free(cmd);
    }
    g_app_launch_context_get_startup_notify_id(ctx, app, NULL);
    error = NULL;
    retval = g_app_info_launch (app, NULL, ctx, &error);
}
