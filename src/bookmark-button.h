#ifndef BOOKMARKBUTTON_H
#define BOOKMARKBUTTON_H

#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QPixmap>
#include <QPoint>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QString>
#include <QGraphicsProxyWidget>
#include "mnb-launcher-application.h"
#include "favorite-widget.h"

#define BOOKMARK_BUTTON_SIZE 40
#define BOOKMARK_BUTTON_MARGINS 0
#define BOOKMARK_BUTTON_REMOVE_SIZE 11
#define BOOKMARK_BUTTON_IMAGE_SIZE 32

class BookmarkButton : public QPushButton
{
    Q_OBJECT
public:
                            BookmarkButton(QWidget *parent, QWidget *resizer);
    MnbLauncherApplication* getApplication() const;
    void                    setApplication(MnbLauncherApplication*);
    void                    setPos(QPoint*);
    void                    setFontMetrics(QFontMetrics *);
    void                    set_sub_widget(QLabel *);
    void                    set_animation_widget(QLabel *);
protected:
    void                    enterEvent(QEvent *);
    void                    leaveEvent(QEvent *);
    void                    mouseReleaseEvent(QMouseEvent *);
    void                    launch_favorite();
private:
    MnbLauncherApplication  *application;
    QPixmap                 *image;
    QLabel                  *widget_image;
    QVBoxLayout             *button_layout;
    QPushButton             *remove_button;
    QPoint                  *button_pos;
    QFontMetrics            *font_metrics;
    QWidget                 *resizer;
    QLabel                  *sub_widget;
    QString                 application_name;
    QLabel                  *animation_widget;
    QPropertyAnimation      *anim_enter;
    QPropertyAnimation      *anim_leave;
    QParallelAnimationGroup *group_launch;
    QPropertyAnimation      *anim_launch_geometry;
    QPropertyAnimation      *anim_launch_opacity;
    
signals:
    void                    button_removed(const gchar *);

public slots:
    void                    slot_delete_favorite();
    void                    slot_launch_animation_finished();
};

#endif // BOOKMARKBUTTON_H
