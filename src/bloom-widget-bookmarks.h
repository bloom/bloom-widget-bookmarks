#ifndef BLOOM_WIDGET_BOOKMARKS_H_
#define BLOOM_WIDGET_BOOKMARKS_H_

#include <QtCore>
#include <QtGui>
#include <QUrl>
#include <QtDBus/QDBusObjectPath>

class FavoriteWidget;

class BloomWidgetBookmarks : public QObject
{
    Q_OBJECT

public:
    BloomWidgetBookmarks();
    ~BloomWidgetBookmarks();

public Q_SLOTS:
    int create(int container, const QString &gconf);

protected:
    FavoriteWidget *favorite_widget;
};

#endif /* BLOOM_WIDGET_BOOKMARKS_H_ */
